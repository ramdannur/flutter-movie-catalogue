import 'package:flutter_movie_catalogue/model/movie_response.dart';
import 'package:flutter_movie_catalogue/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject =
  BehaviorSubject<MovieResponse>();

  getNowplaying() async {
    MovieResponse response = await _repository.getNowplayingMovie();
    _subject.sink.add(response);
  }

  getUpcoming() async {
    MovieResponse response = await _repository.getUpComingMovie();
    _subject.sink.add(response);
  }

  getSearch(String keyword) async {
    MovieResponse response = await _repository.getSearchMovie(keyword);
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject => _subject;
}

final bloc = HomeBloc();