import 'package:flutter_movie_catalogue/model/movie.dart';
import 'package:flutter_movie_catalogue/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class DetailBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<Movie> _subject = BehaviorSubject<Movie>();

  getMovie(String id) async {
    Movie response = await _repository.getMovieById(id);
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<Movie> get subject => _subject;
}

final bloc = DetailBloc();