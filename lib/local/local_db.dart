import 'dart:async';

import 'package:flutter_movie_catalogue/model/movie.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class LocalDb {
  List<Movie> _items = [];

  List<Movie> get items {
    return [..._items];
  }

  static const tableMovies = 'movies';

  Future<Database> openDb() async {
    return openDatabase(
      join(await getDatabasesPath(), 'movies_database.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE IF NOT EXISTS $tableMovies(id TEXT PRIMARY KEY, title TEXT, description TEXT, image TEXT, price REAL)',
        );
      },
      // Version provides path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  //Inserting demo data into database
  void initDB() async {
    var prod1 = Movie(
      "1",
      'Shoes',
      'Rainbow Shoes',
      'https://image.tmdb.org/t/p/w342/1UCOF11QCw8kcqvce8LKOO6pimh.jpg',
    );

    var prod2 = Movie(
      "2",
      'Dress',
      'Summer Dress',
      'https://image.tmdb.org/t/p/w342/1UCOF11QCw8kcqvce8LKOO6pimh.jpg',
    );

    // Insert a movie into the database.
    await insertMovie(prod1);

    await insertMovie(prod2);
  }

  Future<void> insertMovie(Movie movie) async {
    // Get a reference to the database.
    final Database db = await openDb();

    // Insert the Movie into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same movie is inserted
    // multiple times, it replaces the previous data.
    await db.insert(
      tableMovies,
      {
        'id': movie.id,
        'description': movie.overview,
        'title': movie.title,
        'image': movie.posterPath,
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Movie>> allMovies() async {
    // Get a reference to the database.
    final Database db = await openDb();

    // Query the table for all The Movies.
    final List<Map<String, dynamic>> maps = await db.query(tableMovies);

    _items.clear();
    _items = List.generate(maps.length, (i) {
      return Movie(maps[i]['id'], maps[i]['title'], maps[i]['description'],
          maps[i]['image']);
    });

    return _items;
  }

  Future<List<Movie>> moviesById(String id) async {
    // Get a reference to the database.
    final Database db = await openDb();

    // Query the table for all The Movies.
    final List<Map<String, dynamic>> maps = await db.query(
      tableMovies,
      where: "id = ?",
      whereArgs: [id],
    );

    return List.generate(maps.length, (i) {
      return Movie(maps[i]['id'], maps[i]['title'], maps[i]['description'],
          maps[i]['image']);
    });
  }

  Future<void> updateMovie(Movie movie) async {
    // Get a reference to the database.
    final db = await openDb();

    // Update the given Movie.
    await db.update(
      tableMovies,
      {
        'id': movie.id,
        'description': movie.overview,
        'title': movie.title,
        'image': movie.posterPath,
      },
      // Ensure that the Movie has a matching id.
      where: "id = ?",
      // Pass the Movies's id as a whereArg to prevent SQL injection.
      whereArgs: [movie.id],
    );
  }

  Future<void> deleteMovie(String id) async {
    // Get a reference to the database.
    final db = await openDb();

    // Remove the Movie from the database.
    await db.delete(
      tableMovies,
      // Use a `where` clause to delete a specific movie.
      where: "id = ?",
      // Pass the Movies's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
}
