import 'package:flutter/material.dart';
import 'pages/movie_detail.dart';
import 'pages/home_widget.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        title: 'My Movie Catalogue',
        //ROUTING UNTUK MENGATUR SETIAP PAGE YANG DI-LOAD
        routes: {
          '/': (ctx) => Home(),
          '/detail': (ctx) => MovieDetailPage()
        },
    );
  }
}
