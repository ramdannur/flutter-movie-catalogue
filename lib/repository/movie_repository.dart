import 'package:flutter_movie_catalogue/model/movie.dart';
import 'package:flutter_movie_catalogue/model/movie_response.dart';
import 'package:flutter_movie_catalogue/network/api_provider.dart';

class MovieRepository{
  ApiProvider _apiProvider = ApiProvider();

  Future<MovieResponse> getNowplayingMovie(){
    return _apiProvider.getNowPlayingMovie();
  }

  Future<MovieResponse> getUpComingMovie(){
    return _apiProvider.getUpComingMovie();
  }

  Future<MovieResponse> getSearchMovie(String keyword){
    return _apiProvider.searchMovie(keyword);
  }

  Future<Movie> getMovieById(String id){
    return _apiProvider.findById(id);
  }
}