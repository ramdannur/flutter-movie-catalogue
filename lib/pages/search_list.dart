import 'package:flutter/material.dart';
import 'package:flutter_movie_catalogue/bloc/home_bloc.dart';
import 'package:flutter_movie_catalogue/model/movie_response.dart';
import 'dart:convert';
import '../components/movie_cell.dart';

class SearchListPage extends StatefulWidget {
  SearchListPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SearchListPage createState() => new _SearchListPage();
}

class _SearchListPage extends State<SearchListPage> {
  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items = List<String>();

  @override
  void initState() {
    items.addAll(duplicateItems);
    bloc.getUpcoming();
    super.initState();
  }

  void filterSearchResults(String query) {
    List<String> dummySearchList = List<String>();
    dummySearchList.addAll(duplicateItems);
    if(query.isNotEmpty) {
      List<String> dummyListData = List<String>();
      dummySearchList.forEach((item) {
        if(item.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Future searchMovie(String keyword) async {
      bloc.getSearch(keyword);
    }

    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  searchMovie(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0))
                    )
                ),
              ),
            ),
            Expanded(
                child: StreamBuilder<MovieResponse>(
                    stream: bloc.subject.stream,
                    builder: (context, AsyncSnapshot<MovieResponse> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        if (snapshot.error != null) {
                          return Center(
                            child: Text("Error Loading Data"),
                          );
                        } else {
                          return ListView.builder(
                            itemBuilder: (BuildContext ctx, int i) {
                              return new FlatButton(
                                onPressed: () => {
                                  Navigator.of(context).pushNamed('/detail', arguments: snapshot.data.items[i].id)
                                },
                                child: new MovieCell(snapshot.data,i),
                                padding: const EdgeInsets.all(0.0),
                                color: Colors.white,
                              );
                            },
                            itemCount: snapshot.data.items.length,
                          );
                        }
                      }
                    }
                )
            ),
          ],
        ),
      ),
    );
  }
}
