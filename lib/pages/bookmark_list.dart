import 'package:flutter/material.dart';
import '../local/local_db.dart';
import '../components/movie_cell.dart';

class BookmarkListPage extends StatelessWidget {
  LocalDb localDb = LocalDb();

  @override
  Widget build(BuildContext context) {
    Future<void> _refreshData(BuildContext context) async {
      await localDb.allMovies();
    }

    return Scaffold(
        body: RefreshIndicator(
            onRefresh: () => _refreshData(context),
            child: FutureBuilder(
                future: localDb.allMovies(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (snapshot.error != null) {
                      return Center(
                        child: Text("Error Loading Data."),
                      );
                    } else {
                      if (localDb.items.length != 0) {
                        return ListView.builder(
                          itemBuilder: (BuildContext ctx, int i) {
                            return new FlatButton(
                              onPressed: () => {
                                Navigator.of(context).pushNamed('/detail',
                                    arguments: snapshot.data[i].id)
                              },
                              child: new MovieCell(localDb, i),
                              padding: const EdgeInsets.all(0.0),
                              color: Colors.white,
                            );
                          },
                          itemCount: localDb.items.length,
                        );
                      } else {
                        return Center(
                          child: Text("Empty Data."),
                        );
                      }
                    }
                  }
                })));
  }
}
