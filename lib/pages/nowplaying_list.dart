import 'package:flutter/material.dart';
import 'package:flutter_movie_catalogue/bloc/home_bloc.dart';
import 'package:flutter_movie_catalogue/model/movie_response.dart';
import '../components/movie_cell.dart';

class NowplayingListPage extends StatefulWidget {
  @override
  _NowplayingListPage createState() => new _NowplayingListPage();
}

class _NowplayingListPage extends State<NowplayingListPage> {
  @override
  void initState() {
    super.initState();
    bloc.getNowplaying();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _refreshData(BuildContext context) async {
      bloc.getNowplaying();
    }

    return Scaffold(
        body: RefreshIndicator(
            onRefresh: () => _refreshData(context),
            child: StreamBuilder<MovieResponse>(
                stream: bloc.subject.stream,
                builder: (context, AsyncSnapshot<MovieResponse> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (snapshot.error != null) {
                      return Center(
                        child: Text("Error Loading Data."),
                      );
                    } else {
                      return ListView.builder(
                        itemBuilder: (BuildContext ctx, int i) {
                          return new FlatButton(
                            onPressed: () => {
                              Navigator.of(context).pushNamed('/detail',
                                  arguments: snapshot.data.items[i].id)
                            },
                            child: new MovieCell(snapshot.data, i),
                            padding: const EdgeInsets.all(0.0),
                            color: Colors.white,
                          );
                        },
                        itemCount: snapshot.data.items.length,
                      );
                    }
                  }
                })));
  }
}
