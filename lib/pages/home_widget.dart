import 'package:flutter/material.dart';
import 'package:flutter_movie_catalogue/pages/bookmark_list.dart';
import 'package:flutter_movie_catalogue/pages/nowplaying_list.dart';
import 'package:flutter_movie_catalogue/pages/search_list.dart';
import 'package:flutter_movie_catalogue/pages/upcoming_list.dart';
import '../components/placeholder_widget.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    new NowplayingListPage(),
    new UpcomingListPage(),
    new BookmarkListPage(),
    new SearchListPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Apps'),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex, // this will be set when a new tab is tapped
        onTap: onTabTapped,
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.event),
            title: new Text('Now Playing'),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.update),
            title: new Text('Up Coming'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text('Favorite')
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('Search Movie')
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}