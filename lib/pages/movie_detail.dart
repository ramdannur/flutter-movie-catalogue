import 'package:flutter/material.dart';
import 'package:flutter_movie_catalogue/bloc/detail_bloc.dart';
import 'package:flutter_movie_catalogue/model/movie.dart';
import '../components/arc_banner_image.dart';
import '../local/local_db.dart';

class MovieDetailPage extends StatefulWidget {
  @override
  _MovieDetailPage createState() => new _MovieDetailPage();
}

class _MovieDetailPage extends State<MovieDetailPage> {
  String id;
  Movie item;

  var _isFavorited = false;

  final localDb = LocalDb();

  @override
  Widget build(BuildContext context) {
    id = ModalRoute.of(context).settings.arguments as String;
    bloc.getMovie(id);

    Future<void> unBookmarkMovie(BuildContext context, String id) async {
      await localDb.deleteMovie(id);
    }

    Future<void> bookmarkMovie(BuildContext context, Movie item) async {
      await localDb.insertMovie(Movie(
        item.id,
        item.title,
        item.overview,
        item.posterPath,
      ));
    }

    return Scaffold(
        appBar: AppBar(title: Text('Detail'), actions: <Widget>[
          Container(
              child: new FutureBuilder<List<Movie>>(
                  future: localDb.moviesById(id), // a Future<String> or null
                  builder: (context, snapshot) {
                    _isFavorited = false;
                    if (snapshot.hasData) {
                      if (snapshot.data.length >= 1) _isFavorited = true;
                    }
                    return new IconButton(
                      icon: Icon(
                        _isFavorited == true
                            ? Icons.favorite
                            : Icons.favorite_border,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        if (item != null) {
                          if (_isFavorited == true) {
                            unBookmarkMovie(context, id);
                            setState(() {
                              _isFavorited = false;
                            });
                          } else {
                            bookmarkMovie(context, item);
                            setState(() {
                              _isFavorited = true;
                            });
                          }
                          print(_isFavorited);
                        }
                      },
                    );
                  }))
        ]),
        body: Container(
            child: StreamBuilder<Movie>(
          stream: bloc.subject.stream,
          builder: (context, AsyncSnapshot<Movie> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return new Center(child: CircularProgressIndicator());
              default:
                if (snapshot.hasError) {
                  return new Center(child: Text('Error: ${snapshot.error}'));
                } else if (snapshot.hasData) {
                  item = snapshot.data;

                  return new Column(children: [
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 80.0),
                          child: ArcBannerImage(item.bannerPath),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 16.0,
                          right: 16.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Material(
                                borderRadius: BorderRadius.circular(4.0),
                                elevation: 2.0,
                                child: Image.network(
                                  item.posterPath,
                                  fit: BoxFit.cover,
                                  width: 0.7 * 140.0,
                                  height: 140.0,
                                ),
                              ),
                              SizedBox(width: 16.0),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    item.title,
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                  SizedBox(height: 8.0),
                                ],
                              )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          item.overview,
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ]);
                } else {
                  return new Center(child: Text('Result: No data'));
                }
            }
          },
        )));
  }
}
