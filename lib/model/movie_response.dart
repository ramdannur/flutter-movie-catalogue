import 'movie.dart';

class MovieResponse {
  final List<Movie> items;
  final String error;

  MovieResponse(this.items, this.error);

  MovieResponse.fromJson(Map<String, dynamic> json)
      : items =
  (json["results"] as List).map((i) => new Movie.fromJson(i)).toList(),
        error = "";

  MovieResponse.withError(String errorValue)
      : items = List(),
        error = errorValue;
}