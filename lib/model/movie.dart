class Movie {
  final String id;
  final String title;
  final String overview;
  final String posterPath;
  final String bannerPath;

  Movie(this.id, this.title, this.overview, this.posterPath) : bannerPath = "";

  Movie.fromJson(Map<String, dynamic> json)
      : id = (json["id"].toString() ?? ""),
        title = (json['title'] ?? ""),
        overview = (json['overview'] ?? ""),
        posterPath = "https://image.tmdb.org/t/p/w342/" + (json['poster_path'] ?? ""),
        bannerPath = "https://image.tmdb.org/t/p/w780/" + (json['backdrop_path'] ?? "");

}