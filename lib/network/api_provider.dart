import 'package:dio/dio.dart';
import 'package:flutter_movie_catalogue/model/movie.dart';
import 'package:flutter_movie_catalogue/model/movie_response.dart';
import 'package:flutter_movie_catalogue/utils/logging_interceptor.dart';

class ApiProvider {
  final String _endpoint = "https://api.themoviedb.org/3/";
  final String _apiKey = "6a631cfb34027be57967a27d800e3061";
  Dio _dio;

  ApiProvider() {
    BaseOptions options = BaseOptions(receiveTimeout: 5000, connectTimeout: 5000);
    _dio = Dio(options);
    _dio.interceptors.add(LoggingInterceptor());
  }

  Future<MovieResponse> getNowPlayingMovie() async {
    try {
      Response response = await _dio.get(_endpoint + "movie/now_playing?api_key=" + _apiKey);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return MovieResponse.withError(_handleError(error));
    }
  }

  Future<MovieResponse> getUpComingMovie() async {
    try {
      Response response = await _dio.get(_endpoint + "movie/upcoming?api_key=" + _apiKey);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return MovieResponse.withError(_handleError(error));
    }
  }

  Future<MovieResponse> searchMovie(String keyword) async {
    try {
      Response response = await _dio.get(_endpoint + "search/movie?query=" + keyword + "&api_key=" + _apiKey);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return MovieResponse.withError(_handleError(error));
    }
  }

  Future<Movie> findById(String id) async {
    try {
      Response response = await _dio.get(_endpoint + "movie/" + id + "?api_key=" + _apiKey);
      return Movie.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      // return MovieResponse.withError(_handleError(error));
    }
  }

  String _handleError(Error error) {
    String errorDescription = "";
    if (error is DioError) {
      DioError dioError = error as DioError;
      switch (dioError.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription =
          "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.RESPONSE:
          errorDescription =
          "Received invalid status code: ${dioError.response.statusCode}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "Send timeout in connection with API server";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return errorDescription;
  }
}